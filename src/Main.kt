import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

fun main()= runBlocking<Unit> {
    val myFlow = flow<Int> {
        (0 until 5).forEach { value ->
            delay(100)
            emit(value * 100)
        }
    }

    println("let's start")

    myFlow
        .map { value -> value * value }
        .collect { value -> println(value) }

    println("finish")
}
