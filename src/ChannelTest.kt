import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow

val myChannel = Channel<Int>()
fun main() {
    CoroutineScope(Job()).launch {
        repeat(Int.MAX_VALUE) {
            if(myChannel.isClosedForSend) {
                this.cancel()
            }
            myChannel.send(it)
            delay(100)
        }
    }

    runBlocking {
        myChannel.consumeAsFlow().collect { value ->
            println(value)

            if(value == 50)
                throw Throwable("oops")
        }
    }

    println("finish")
}